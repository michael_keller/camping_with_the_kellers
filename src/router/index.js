import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

console.log(`Environment: ${process.env.NODE_ENV}`)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('@/views/About.vue')
  },
  {
    path: '/article/:id',
    name: 'Article',
    component: () => import('@/views/Article.vue')
  },
  {
    path: '/map/:id',
    name: 'Article Map',
    component: () => import('@/views/ArticleMap.vue')
  },
  {
    path: '/map/',
    name: 'Map',
    component: () => import('@/views/Map.vue')
  }
]

if(process.env.NODE_ENV === 'production'){
  routes.forEach(route => {
    route.path = `/camping_with_the_kellers${route.path}`
  })
}

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
